<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $table = "users";
    public function index()
    {
        $user = User::select('users.*')
                    ->paginate(50);
        // dd($user);
        return view($this->table.'.index', [
            'table' =>  $this->table,
            'title' =>'Listado de Usuarios',
            'data'  => $user
        ]);
    }

    public function search(Request $r)
    {
        if(!isset($r->txtSearch) || strlen(trim($r->txtSearch)) == 0)
            return redirect()->back()->with('danger', 'Debe llenar el campo para buscar');

        return view($this->table.'.index', [
            'table'     =>  $this->table,
            'title'     => 'Listado de Usuarios',
            'txtSearch' => $r->txtSearch,
            'data'      => User::select('id', 'name','email','tipo_usuario')
                                ->where('name', 'like', '%'.$r->txtSearch.'%')
                                ->paginate(50)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view($this->table.'.create', [
            'table' =>  $this->table,
            'title'=>'Agregar Usuarios'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user               = new User();
        $user->name         = $request->name;
        $user->email        = $request->email;
        $user->tipo_usuario = $request->tipo_usuario;
        $user->password     = Hash::make($request->password);
        $e                  = $user->save();

        return redirect()->route($this->table.'.index')
                ->with(($e)?'info':'danger',($e)?'Guardado con exito':'Ocurrio un problema al guardar al usuario intente de nuevo.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = User::findOrfail($id);
        return view($this->table.'.edit', [
            'table' =>  $this->table, 
            'title'=>'Actualizar Usuario',
            'data'=> $users
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,User $users, $id)
    {
        $user               = $users::findOrfail($id);
        $user->name         = $request->name;
        $user->email        = $request->email;
        $user->tipo_usuario = $request->tipo_usuario;
        $user->password     = Hash::make($request->password);
        $e                  = $user->save();
        return redirect()->route($this->table.'.index')
                        ->with(($e)?'info':'danger',($e)?'Se edito un registro con exito. ':'Ocurrio un problema al editar el rubro intente de nuevo.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $users = User::findOrfail($id);
        $m = $users->delete();
        return redirect()->route($this->table.'.index')
                ->with(($m)?'info':'danger',($m)?'Se borro un registro correctamente':'Ocurrio un error al borrar el registro... Intente de nuevo');
    }
}
