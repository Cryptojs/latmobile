<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::check()){
            $user = Auth::user();
            switch ($user->tipo_usuario) {
                case 700://admin
                    return redirect()->route('users.index');
                break;
                case 711://user
                    return redirect()->route('users.index');
                break;
            }
        } else return redirect()->route('home');
    }
}
